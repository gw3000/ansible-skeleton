## Installing Ansible
```console
$ apt-get update && apt-get install ansible ansible-lint
```
## Adding folder structure for logfiles
```console
$ bash logs.sh
```

## SSH-Keygen

```console
$ ssh-keygen -t ed25519 -a 420 -f ~/.ssh/ansible.ed25519 -C "ansible key"
```

### starting ssh-agent
```console
$ eval `ssh-agent`
```

### adding ssh-key
```console
$ ssh-add ~/.ssh/ansible.ed25519
```

### list all ssh-keys loaded in agent
```console
$ ssh-add -l
```